//
//  TransitionViewController.swift
//  SlideViewController
//
//  Created by john on 8/31/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class TransitionViewController: UIViewController {

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    var slideDelegate:SlideButtonViewDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backgroundView.backgroundColor = UIColor.init(hexString:slideDelegate.getSelectedColor())
        self.titleLabel?.text = slideDelegate.getSelectedTitle()

    }
    

    @IBAction func returnToMain(_ sender: Any) {
        
        dismiss(animated: true) 
    }
   

}
