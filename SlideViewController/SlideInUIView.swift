//
//  SlideInUIView.swift
//  SlideViewController
//
//  Created by john on 8/31/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class SlideUIView: UIView {

    var ROW_HEIGHT:CGFloat = 50.0
    var OFFSET:CGFloat = 10.0
    var buttons:[UIButton]!
    var titles:[String] = ["best selling", "rock", "hip-hop/rap","alternative","electronic","metal","experimental","pop","jazz","blues","punk","r&b/soul","more"]
    var colors:[String] = ["FF7F33","E84A2E","FF4173", "E42EE8", "A330FF", "5551E8", "5994FF", "5DC7E8","5FFFAA", "51E889", "FFD952", "E86735", "FF334A"]
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButtons()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButtons()
    }
    
    private func setupButtons(){
        buttons = [UIButton]()
        
        for (count,title) in titles.enumerated(){
            
            let button = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: CGFloat(frame.width), height: CGFloat(ROW_HEIGHT)))
            
            button.titleLabel?.text = title
            button.setTitle(title, for: .normal)
            button.contentHorizontalAlignment = .left
            button.titleEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 10.0, bottom: 0.0, right: 0.0)
            button.titleLabel?.textColor = UIColor.white
            button.backgroundColor = UIColor.init(hexString:colors[count])
            
            buttons.append(button)
            addSubview(button)
           
            button.center = CGPoint.init(x: CGFloat(frame.width/2), y: (CGFloat(count + 1) * (ROW_HEIGHT + OFFSET)) - (ROW_HEIGHT/2))
            print(button.center)
            
            button.alpha = 0.0
        }
        
        
    }
    
    func animateButtons(){
        
        for button in buttons{
            UIView.animate(withDuration: 0.4) {
                button.alpha = 1.0
               
            }
        }
    }
}


