//
//  SlideButtonsView.swift
//  SlideViewController
//
//  Created by john on 8/31/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class SlideButtonsView: UIView {
    
    var ROW_HEIGHT:CGFloat = 50.0
    var OFFSET:CGFloat = 10.0
    var buttons:[UIButton]!
    var titles:[String]!
    var colors:[String]!
    var selectedTitle:String?
    var selectedColor:String?
    
    var triggerSegue: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titles = [String]()
        colors = [String]()
        setupButtons()
        
    }
    
    init(titles:[String], colors: [String], rowHeight:CGFloat, initialGapBetweenButtons:CGFloat) {
       
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: CGFloat(rowHeight * CGFloat(titles.count))))
        
        self.titles = titles
        self.colors = colors
        self.ROW_HEIGHT = rowHeight
        self.OFFSET = initialGapBetweenButtons
        setupButtons()
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        titles = [String]()
        colors = [String]()
        setupButtons()
    }
    
    
    /**
     Sets up the rows in the list, positioning them for animation.
     */
    
    private func setupButtons(){
        
        buttons = [UIButton]()
        
        // Base number of rows/buttons on number of titles
        for (count,title) in titles.enumerated(){
            
            let button = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: CGFloat(frame.width), height: CGFloat(ROW_HEIGHT)))
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17.0)
            button.titleLabel?.text = title
            button.setTitle(title, for: .normal)
            button.contentHorizontalAlignment = .left
            button.titleEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 10.0, bottom: 0.0, right: 0.0)
            button.titleLabel?.textColor = UIColor.white.withAlphaComponent(1.0)
            
            //Set row to dark gray In case fewer colors are provided than titles
            button.backgroundColor = count < colors.count ? UIColor.init(hexString:colors[count]) : UIColor.darkGray
            
            buttons.append(button)
            addSubview(button)
            
            // Position each row/button a little further apart than the previous loops button spacing
            button.center = CGPoint.init(x: CGFloat(frame.width/2), y: (CGFloat(count + 1) * (ROW_HEIGHT + OFFSET)) - (ROW_HEIGHT/2))
            button.alpha = 0.0
            
            
            // Add the triggers for user interaction
            let tgr = UITapGestureRecognizer.init(target: self, action: #selector(selectRowAndAnimate(_:)))
            button.addGestureRecognizer(tgr)
            
            let lgr = UILongPressGestureRecognizer.init(target: self, action: #selector(enlargeButton(_:)))
            button.addGestureRecognizer(lgr)
            
            
        }

    }
    
    
    /**
    Trigger when user taps to select a row in the list.  Calls the closure specified in
     ViewController to trigger a segue to the TransitionViewController
     
     - Parameter sender: The tap gesture recognizer set on each row of the list
     
     */
    @objc func selectRowAndAnimate(_ sender:UITapGestureRecognizer){
    
        if let button = sender.view as? UIButton{
            if let titleText = button.titleLabel?.text, let bkColor = button.backgroundColor?.toHexString(){
                selectedColor = bkColor
                selectedTitle = titleText
                triggerSegue!()
            }
            
        }
        
    }
    
    
    /**
     Trigger when there's a long press on a specific row. Generates the enlarge animation on
     press and hold and the "ripple" effect when released
     
     - Parameter sender: the long press gesture recognizer set on each row of the list
     */
    
    @objc func enlargeButton(_ sender:UILongPressGestureRecognizer){
        
        // Enlarge the selected row while user holds it
        if sender.state == .began{
            if let button = sender.view as? UIButton{
                UIView.animate(withDuration: 0.3, animations: {
                    button.transform = CGAffineTransform.init(scaleX: 1.02, y: 1.02)
                }, completion:{ _ in

                })
                
            }
         // Start the ripple animation when user lets go
        }else if sender.state == .ended{
            if let button = sender.view as? UIButton{
                
                // Used to provide increasing delay between UIView.animate calls
                var downDelay = 0.1
                var upDelay = 0.1
                
                UIView.animate(withDuration: 0.3, animations: {
                    // reduce the button back to normal size
                    button.transform = CGAffineTransform.identity
                }, completion:{_ in
                    
                    // Get the index of the selected button in the buttons array
                    if let pressedButtonIndex = self.buttons.firstIndex(of: button){
   
                        // Ripple Down from selected button
                        // Start effect one past selected row to the end of the buttons array
                        for downIndex in Int(pressedButtonIndex + 1)..<self.buttons.count{
                            
                            UIView.animate(withDuration: 0.5, delay: downDelay, animations: {
                                self.buttons[downIndex].transform = CGAffineTransform.init(scaleX: 1.04, y: 1.04)
                            }, completion:{_ in
                                self.buttons[downIndex].transform = CGAffineTransform.identity
                            })
                            
                            // Increase the delay between animation trigger
                            downDelay += 0.05
                        }
                        
                        if pressedButtonIndex != 0{
                            
                            // Ripple Up from selected button
                            
                            // Start effect from one less than selected row down to 0th element of
                            // the buttons array
                            for upIndex in (0...Int(pressedButtonIndex - 1)).reversed(){

                                UIView.animate(withDuration: 0.5, delay: upDelay, animations: {
                                    self.buttons[upIndex].transform = CGAffineTransform.init(scaleX: 1.04, y: 1.04)
                                }, completion: {_ in
                                    self.buttons[upIndex].transform = CGAffineTransform.identity
                                })
                                
                                // Increase the delay between animation trigger
                                upDelay += 0.05
                            }
                        }
                    }
                })
                
            }
        }
        
        
    }
    
    /**
     Performs the main animation when the list is first shown to end user
     */
    func animateButtons(){
        
        for count in 0..<buttons.count{
            
            let button = buttons[count]
            
            // Enlarge each button, animate its alpha from invisible to visible, shift its position
            UIView.animate(withDuration: 0.3, delay:Double(count) * 0.05, animations: {
                button.transform = CGAffineTransform.init(scaleX: 1.02, y: 1.02)
                    button.alpha = 0.8
                
                    // Move the button back so it's directly adjacent to the one above it by
                    // subtracting the offset assigned when initially positioning it
                    button.layer.position = CGPoint(x:button.frame.midX, y:button.frame.midY - CGFloat(CGFloat(count) * self.OFFSET))
                    self.layoutIfNeeded()
            }, completion:{_ in
                UIView.animate(withDuration: 0.3, animations: {
                    button.transform = CGAffineTransform.identity
                })
                
            })

        }
    }
    
    /**
     Repositions each row with offset, should be called prior to animateButtons
     */
    func resetButtons(){
        
        for (count,button) in buttons.enumerated(){
            
            // give each row an increasing offset from the previous
            button.center = CGPoint.init(x: CGFloat(frame.width/2), y: (CGFloat(count + 1) * (ROW_HEIGHT + OFFSET)) - (ROW_HEIGHT/2))
            button.alpha = 0.0
           
        }
    }
    
}


