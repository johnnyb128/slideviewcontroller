//
//  SlideButtonViewDelegate.swift
//  SlideViewController
//
//  Created by john on 8/31/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

protocol SlideButtonViewDelegate{
    
    func getSelectedColor()->String
    func getSelectedTitle()->String
    
}
