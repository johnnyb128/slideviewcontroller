//
//  ViewController.swift
//  SlideViewController
//
//  Created by john on 8/31/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var slideButtonsView: SlideButtonsView!
    
    var slideButtonViewTitles:[String] = ["humpback whale", "killer whale", "pilot whale","blue whale","chimpanzee","orangutan","african elephant", "sea otter", "bottlenose dolphin","gorilla","walrus","narwhal","california sea lion", "polar bear", "octopus"]
    
    var slideButtonViewColors:[String] = ["D98A36","D95A36","F84C3E","E02D53", "F725BC", "D524ED", "8B15D6", "6419F7", "180BE0","0D3EFC", "006AE6", "00B8FE", "0CE4E8", "0DFFC0", "00E868"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Initialize the instance
        slideButtonsView = SlideButtonsView.init(titles: slideButtonViewTitles, colors: slideButtonViewColors, rowHeight: 50, initialGapBetweenButtons: 10)
        
        // assign the callback segue to trigger the transition to TransitionViewController
        // when a user selects a row in SlideButtonsView
        slideButtonsView.triggerSegue = {
            self.performSegue(withIdentifier: "segueToTransition", sender: self)
        }
        
        view.addSubview(slideButtonsView)
        
        // Set the center of the SlideButtonsView to dead center
        slideButtonsView.center = CGPoint.init(x: UIScreen.main.bounds.midX, y: UIScreen.main.bounds.midY)
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Reset the position of the rows before the user can see
        // so the animation is ready to be invoked again
        slideButtonsView.resetButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        // Invoke the animation once the VC is visible
        slideButtonsView.animateButtons()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let tvc = segue.destination as? TransitionViewController{
            // Set the SlideButtonViewDelegate of the TransitionVC to self so the
            // transitioned-to VC can acqire the color and title of the selected row
            tvc.slideDelegate = self
        }
    }

}

// Implement the delegate that grabs the selected row title and color
extension ViewController: SlideButtonViewDelegate{
    func getSelectedColor() -> String {
        return self.slideButtonsView.selectedColor!
    }
    
    func getSelectedTitle() -> String {
        return self.slideButtonsView.selectedTitle!
    }
    
    
}
